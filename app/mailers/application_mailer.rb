class ApplicationMailer < ActionMailer::Base
  include ActionView::Helpers::TextHelper
  helper EmailHelper

  # default from: "no-reply@samarbeid.org" # configured in environments/production.rb via ENV
  layout "mailer"
end
