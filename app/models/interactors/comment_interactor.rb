class Interactors::CommentInteractor
  def self.create(message, object, user)
    html_without_mention_labels, mentions = Services::Mentioning.extract_mentions(Services::EditorSanitizer.sanitize(message))
    user_mention_ids = Services::Mentioning.get_user_mentions(mentions)
    mentioned_users = User.where(id: user_mention_ids)

    comment = Comment.new(message: html_without_mention_labels, author: user, object: object)

    if comment.save
      Events::CommentedEvent.create!(subject: user, comment: comment, data: {mentioned_users: mentioned_users})

      if object.respond_to?(:contributors)
        add_contributor_if_needed(object, user)
      end
    end

    comment
  end

  def self.add_contributor_if_needed(object, contributor)
    object.contributors << contributor unless object.contributors.include?(contributor)
  end
  private_class_method :add_contributor_if_needed
end
