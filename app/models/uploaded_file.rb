class UploadedFile < ApplicationRecord
  has_one_attached :file
  validates :file, size: {less_than: 100.megabytes, message: "Datei muss kleiner als 100 Mb sein!"}

  before_save :to_text

  def title
    return read_attribute(:title) unless read_attribute(:title).blank?
    file.filename.to_s
  end

  def search_data(prefix = nil)
    {title: title,
     file_name: filename_for_search,
     file_content: to_text}.transform_keys { |key| "#{prefix}#{UploadedFile.human_attribute_name(key)}" }
  end

  def self.destroy_invalid
    all.where("created_at < ?", 2.days.ago).find_each.reject do |uploaded_file|
      ContentItem.where(content_type: ContentTypes::File.to_s).select { |ci| ci.value&.include?(uploaded_file) }.any? ||
        DossierFieldDefinition.where(content_type: ContentTypes::File.to_s).select { |dfd|
          dfd.definition&.dossiers&.select { |d| d.get_field_value(dfd.id)&.include?(uploaded_file) }&.any?
        }.any?
    end.each(&:destroy)
  end

  private

  def to_text
    return file_as_text if file_as_text

    begin
      content_type = file.blob.content_type
      file_content = file.download
    rescue
      return unless attachment_changes["file"]
      # This is a hack. Because ActiveStorage enqueues a job to actually process the attached file we can not access the blob/file for a long time
      # One way around for now (there exist no callbacks - see for example https://github.com/rails/rails/issues/35044)
      attachable = attachment_changes["file"].attachable

      tmp_file_attachment = attachable.is_a?(Hash) ? attachable[:io] : attachable.to_io
      return unless tmp_file_attachment
      file_content = File.read tmp_file_attachment
      content_type = IO.popen(
        ["file", "--brief", "--mime-type", tmp_file_attachment.path],
        in: :close, err: :close
      ) { |io| io.read.chomp }
    end

    rest_client_response = RestClient::Request.execute(method: :put, url: Tika::Configuration.new.url,
      headers: {"Content-Type": content_type},
      payload: file_content)
    file_as_utf_text = rest_client_response.force_encoding("UTF-8").to_s
    text_limited_to_bible_length = file_as_utf_text.truncate(5_000_000, omission: " [length limited to 5 million chars]")
    self.file_as_text = text_limited_to_bible_length
  end

  def filename_for_search
    filename = file.blob.filename.to_s
    searchable_filename = filename.gsub(/[.\-_]/, " ")
    [filename, searchable_filename].join("\n")
  end
end
