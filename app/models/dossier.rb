class Dossier < ApplicationRecord
  include SearchForIndexWithCategories
  include ReferenceSupport

  belongs_to :definition, class_name: "DossierDefinition", inverse_of: :dossiers
  belongs_to :created_by, class_name: "User", optional: true
  has_many :comments, as: :object, dependent: :destroy
  has_many :events, as: :object, dependent: :destroy

  scope :not_deleted, -> { where(deleted: false) }

  validates :definition_id, presence: true
  validate :validate_unchangeable_fields, on: :update
  validate :validate_data, unless: -> { definition.nil? }

  after_save :reindex_workflows

  searchkick language: "german", callbacks: :async, word_middle: [:identifier, :title, :subtitle], suggest: [:title, :subtitle]
  scope :search_import, -> { where(deleted: false) }

  def should_index?
    !deleted
  end

  def field_data=(new_field_data)
    new_field_data.each { |key, value| set_field_value(key, value) } if new_field_data.is_a?(Hash)
  end

  def get_field_value(id)
    field_definitions[id.to_s].content_type.deserialize(data[id.to_s])
  end

  def set_field_value(id, value)
    field_definition = field_definitions[id.to_s]

    if field_definition
      data[id.to_s] = field_definition.content_type.serialize(field_definition.content_type.cast(value))
      touch if persisted?
    end
  end

  def remove_field_value(id)
    data.delete(id.to_s)
  end

  def data_fields(required_and_recommended_only = false)
    definition_fields = definition.fields
    definition_fields = definition_fields.required_or_recommended if required_and_recommended_only
    definition_fields.map { |field| {definition: field, value: get_field_value(field.id)} }
  end

  def title
    field_values_to_text(definition.title_fields, [String(id), definition.name]) if definition
  end

  def subtitle
    field_values_to_text(definition.subtitle_fields, []) if definition
  end

  def identifier
    "*#{id}"
  end

  def reference_label(noaccess = false)
    identifier + " (#{definition.name})" + (deleted? ? "" : " • #{title}")
  end

  def mention_label(noaccess = false)
    reference_label
  end

  # TODO: Improve Performance (#671)
  def referenced_in
    ContentItem.where(content_type: ContentTypes::Dossier.to_s).select { |ci| ci.value&.include?(self) }.map(&:workflow).uniq
  end

  private

  def field_definitions
    return @field_definitions unless @field_definitions.nil?

    @field_definitions = {}
    definition.fields.each do |field_definition|
      @field_definitions[field_definition.id.to_s] = field_definition
    end

    @field_definitions
  end

  def validate_unchangeable_fields
    errors.add(:definition, "kann nicht geändert werden") if definition_id_changed?
    errors.add(:created_by, "kann nicht geändert werden") if created_by_id_changed?
  end

  def validate_data
    if data&.is_a?(Hash)
      definition.fields.map { |field| field.validate(self) }
    else
      errors.add(:data, "muss ein Hash sein")
    end
  end

  def field_values_to_text(field_ids, default_values)
    elements = if field_ids.any?
      field_ids.map { |field_id| field_definitions[field_id.to_s].localized_string(get_field_value(field_id)) }
    else
      default_values
    end

    elements.reject { |element| element.empty? }.join(" • ").squish
  end

  def search_data
    {
      title: title&.downcase,
      subtitle: subtitle,
      identifier: [identifier, id.to_s],
      definition_id: definition.id,
      definition: definition.name,
      created_at: created_at,
      updated_at: updated_at
    }.merge(data_fields_search_data).merge(visibility_for_groups)
  end

  def visibility_for_groups
    {visible_for_groups: definition.groups.map(&:id)}
  end

  def data_fields_search_data
    data_fields.reject { |df| df[:value].nil? }.map { |df| data_field_search_data(df) }.reduce({}, :merge)
  end

  def data_field_search_data(data_field)
    name = data_field[:definition].name
    value = data_field[:value]
    case data_field[:definition].content_type.name
    when ContentTypes::File.name
      value.map { |doc| doc.search_data("#{name} - ") }.reduce({}, :merge)
    else
      {name => data_field[:definition].localized_string(value)}
    end
  end

  def reindex_workflows
    referenced_in.uniq.each(&:reindex)
  end
end
