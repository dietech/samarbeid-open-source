class Events::ChangedDataEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  private

  def validate_event_type_values
    validate_object_is_one_of(ContentItem)
  end
end
