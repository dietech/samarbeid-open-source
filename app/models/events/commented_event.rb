class Events::CommentedEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  def mentioned_users
    data&.[](:mentioned_users)
  end

  def notification_receivers
    receivers = []
    receivers |= [object.assignee] if object.respond_to?(:assignee)
    receivers |= object.contributors if object.respond_to?(:contributors)

    (receivers | mentioned_users).compact.uniq - [subject]
  end

  alias_method :set_object, :object=
  def object=(value)
    raise StandardError, "#{self.class} doesn't allow to set object directly. Use comment setter instead."
  end

  alias_method :get_object, :object
  def object
    comment&.object
  end

  def comment=(value)
    set_object(value)
  end

  def comment
    get_object
  end

  def payload(current_user)
    Services::Mentioning.add_mention_labels(comment.message, current_user)
  end

  private

  def validate_event_type_values
    errors.add(:comment, "should be an #{Comment}") unless comment.is_a?(Comment)
    validate_object_is_one_of(Workflow, Task, Ambition, WorkflowDefinition, Dossier, DossierDefinition)
    errors.add(:data, "value for key :mentioned_users should be an array of users") unless mentioned_users.all? { |u| u.is_a?(User) }
  end

  def avatar(referenced)
    if referenced
      {
        icon: "mdi-comment-text-outline"
      }
    else
      {
        label: subject.avatar_label,
        url: subject.avatar_url
      }
    end
  end
end
