class Events::ChangedDueDateEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  def new_due_date
    data&.[](:new_due_date)
  end

  def old_due_date
    data&.[](:old_due_date)
  end

  alias_method :new_value, :new_due_date
  alias_method :old_value, :old_due_date

  def notification_receivers
    [object.assignee].compact - [subject]
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Task)
    errors.add(:data, "value for key :new_due_date should be a date") unless new_due_date.nil? || new_due_date.is_a?(Date)
    errors.add(:data, "value for key :old_due_date should be a date") unless old_due_date.nil? || old_due_date.is_a?(Date)
  end

  def translate_value(value)
    I18n.l(value)
  end
end
