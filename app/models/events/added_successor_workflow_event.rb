class Events::AddedSuccessorWorkflowEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  def successor_workflow
    data&.[](:successor_workflow)
  end

  alias_method :new_value, :successor_workflow

  def notification_receivers
    []
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Workflow)
    errors.add(:data, "value for key :successor_workflow should be a workflow") unless successor_workflow.is_a?(Workflow)
  end
end
