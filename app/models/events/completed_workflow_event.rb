class Events::CompletedWorkflowEvent < Event
  validate :validate_event_type_values

  def completed_workflow
    data&.[](:completed_workflow)
  end

  alias_method :old_value, :completed_workflow

  def notification_receivers
    ([object.assignee] - [subject]).compact
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Ambition)
    errors.add(:data, "value for key :completed_workflow should be a workflow") unless completed_workflow.is_a?(Workflow)
  end
end
