class DossierDefinition < ApplicationRecord
  include SearchForIndexWithCategories
  include ReferenceSupport

  has_many :fields, -> { order(:position) }, class_name: "DossierFieldDefinition", foreign_key: "definition_id", dependent: :destroy, inverse_of: :definition
  has_many :dossiers, foreign_key: "definition_id", dependent: :destroy, inverse_of: :definition

  has_and_belongs_to_many :groups, after_add: :acl_reindex, after_remove: :acl_reindex

  validates :name, presence: true, uniqueness: {case_sensitive: false}
  validate :validate_title_fields
  validate :validate_subtitle_fields

  searchkick language: "german", callbacks: :async, word_middle: [:name, :description], suggest: [:title, :subtitle]

  # for rails admin
  accepts_nested_attributes_for :fields, allow_destroy: true

  # for rails admin
  def rails_admin_label
    "(##{id}) #{name}"
  end

  def self.defaults_for_search_for_index
    {fields: ["name"], order: {name: :asc}}
  end

  def search_data
    {
      name: name,
      description: description,
      created_at: created_at
    }.merge(visibility_for_groups)
  end

  def visibility_for_groups
    {visible_for_groups: groups.map(&:id)}
  end

  def reference_label(_noaccess)
    name.to_s
  end

  private

  def validate_title_fields
    if title_fields.is_a?(Array)
      title_fields.each do |field_id|
        unless fields.where(id: field_id).exists?
          errors.add(:title_fields, "referenziert das Feld mit der ID (#{field_id}), welches in dieser Dossier Definition nicht existert")
        end
      end
    else
      errors.add(:title_fields, "muss ein Array von Feldern sein")
    end
  end

  def validate_subtitle_fields
    if subtitle_fields.is_a?(Array)
      subtitle_fields.each do |field_id|
        unless fields.where(id: field_id).exists?
          errors.add(:subtitle_fields, "referenziert das Feld mit der ID (#{field_id}), welches in dieser Dossier Definition nicht existert")
        end
      end
    else
      errors.add(:subtitle_fields, "muss ein Array von Feldern sein")
    end
  end

  private

  def acl_reindex(_params)
    RunAclReindexOnlyJob.perform_later
  end
end
