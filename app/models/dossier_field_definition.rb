class DossierFieldDefinition < ApplicationRecord
  include ContentTypeSupport

  belongs_to :definition, class_name: "DossierDefinition", inverse_of: :fields

  validates :name, :position, :content_type, presence: true
  validates :name, format: {without: /\./, message: "darf keine Punkte enthalten"}
  validates :name, uniqueness: {scope: :definition_id, case_sensitive: false}
  validates :position, uniqueness: {scope: :definition_id}
  validate :validate_unchangeable_fields, on: :update
  validate :validate_content_type_changes, on: :update

  scope :required_or_recommended, -> { where(required: true).or(where(recommended: true)) }

  # for rails admin
  def rails_admin_label
    "(##{id}) #{name}"
  end

  def validate(dossier)
    value = dossier.get_field_value(id)

    if required? && content_type.data_empty?(value)
      dossier.errors.add("field_#{id}", "muss ausgefüllt werden")
    end

    if unique? && Dossier.where.not(id: dossier.id).where(definition: definition, deleted: false).where("data @> ?", {id.to_s.to_sym => value}.to_json).exists?
      dossier.errors.add("field_#{id}", "ist bereits vergeben")
    end

    unless content_type.data_empty?(value)
      validation_result = content_type.validate_data(value)
      dossier.errors.add("field_#{id}", validation_result) if validation_result != true
    end
  end

  private

  def validate_unchangeable_fields
    errors.add(:definition, "kann nicht geändert werden") if definition_id_changed?
  end

  def validate_content_type_changes
    if content_type_changed? &&
        (!(content_type_class_for(content_type_was) <= ContentTypes::String) || !(content_type <= ContentTypes::String))
      errors.add(:content_type, "kann nicht geändert werden")
    end
  end
end
