require "active_support/concern"

module ContentTypeSupport
  extend ActiveSupport::Concern

  included do
    enum content_type: ContentType.enum_values

    validates_presence_of :content_type
    validate :validate_options

    def content_type_class_for(content_type)
      result = ContentType.types_hash[content_type.to_sym]
      raise "Unsupported content_type '#{content_type}'" if result.nil?
      result
    end

    def content_type_class
      content_type_class_for content_type_before_type_cast
    end

    alias_method :content_type, :content_type_class

    def localized_string(value)
      return "" if value.nil?

      content_type.localized_string(value, options)
    end

    private

    def validate_options
      if options.is_a?(Hash)
        validation_result = content_type.validate_options(options)
        unless validation_result === true
          validation_result.each do |error_message|
            errors.add(:options, error_message)
          end
        end
      else
        errors.add(:options, "muss ein Hash sein")
      end
    end
  end

  class_methods do
  end
end
