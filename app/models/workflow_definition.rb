class WorkflowDefinition < ApplicationRecord
  include SearchForIndexWithCategories
  include ReferenceSupport

  has_many :workflows, dependent: :destroy
  has_many :comments, as: :object, dependent: :destroy
  has_many :events, as: :object, dependent: :destroy

  has_many :all_task_definitions, -> { order(:position) }, class_name: "TaskDefinition", inverse_of: :workflow_definition, dependent: :destroy
  has_many :direct_task_definitions, -> { order(:position) }, class_name: "TaskDefinition", as: :definition_workflow_or_block
  has_many :block_definitions, -> { order(:position) }, dependent: :destroy
  has_many :content_item_definitions, dependent: :destroy
  has_many :task_item_definitions, dependent: :destroy

  has_and_belongs_to_many :groups, after_add: :acl_reindex, after_remove: :acl_reindex

  scope :not_deleted, -> { where(deleted: false) }
  scope :search_import, -> { not_deleted }

  def item_definitions
    (direct_task_definitions + block_definitions).sort_by(&:position)
  end

  validates :name, presence: true, uniqueness: true

  after_update :reindex_workflows

  searchkick language: "german", callbacks: :async, word_middle: [:name, :description], suggest: [:title, :subtitle]

  def rails_admin_label
    "#{id}-#{name&.parameterize&.truncate(25)}"
  end

  def self.defaults_for_search_for_index
    {fields: ["name"], order: {name: :asc}}
  end

  def search_data
    {
      name: name,
      description: description,
      created_at: created_at
    }.merge(visibility_for_groups)
  end

  def visibility_for_groups
    {visible_for_groups: groups.map(&:id)}
  end

  def build_workflow
    new_workflow = Workflow.new(workflow_definition: self, description: description, definition_version: version)
    new_workflow.content_items = content_item_definitions.not_deleted.map { |content_item_definition| content_item_definition.build_content_item(new_workflow) }
    new_workflow.direct_tasks = direct_task_definitions.not_deleted.map { |def_task| def_task.build_task(new_workflow) }
    new_workflow.blocks = block_definitions.not_deleted.map { |def_block| def_block.build_block(new_workflow) }
    new_workflow
  end

  def create_workflow!
    workflow = build_workflow
    workflow.title = "Test-Workflow"
    workflow.tap(&:save!)
  end

  def structure_as_text
    Services::NewEngine.new(build_workflow).structure_to_s
  end

  def reference_label(_noaccess)
    name.to_s
  end

  private

  def acl_reindex(_params)
    RunAclReindexOnlyJob.perform_later
  end

  def reindex_workflows
    RunReindexForWorkflowsJob.perform_later(self)
  end
end
