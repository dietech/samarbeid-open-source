class ContentTypes::Selection < ContentTypes::Value
  class << self
    def type
      :selection
    end

    def cast(value)
      value
    end

    def localized_string(value, options)
      array_value = Array(value)
      options["items"].select { |item| array_value.include? item["value"] }.map { |item| item["text"] }.join(", ")
    end

    def data_empty?(value)
      super || value.blank?
    end

    def options_schema
      {
        type: "object",
        required: ["items"],
        properties: {
          items: {
            type: "array",
            minItems: 1,
            items: {
              type: "object",
              required: %w[value text],
              properties: {
                value: {
                  type: "integer",
                  format: "auto-increment"
                },
                text: {
                  type: "string"
                }
              }
            }
          },
          multiple: {
            type: "boolean"
          }
        },
        additionalProperties: false
      }
    end
  end
end
