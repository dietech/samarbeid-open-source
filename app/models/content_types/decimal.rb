class ContentTypes::Decimal < ContentTypes::Value
  class << self
    def type
      :decimal
    end

    def cast(value)
      ActiveModel::Type::Decimal.new.cast(value.to_s) unless value.nil?
    end

    def localized_string(value, options)
      ActiveSupport::NumberHelper.number_to_delimited(value)
    end
  end
end
