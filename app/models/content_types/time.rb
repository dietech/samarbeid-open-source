class ContentTypes::Time < ContentTypes::Value
  class << self
    def type
      :time
    end

    def cast(value)
      return value if value.nil?

      val = ActiveModel::Type::Time.new.cast(value).change(year: 2000, month: 1, day: 1)
      def val.as_json(options = nil)
        self&.strftime("%H:%M")
      end

      val
    end

    def localized_string(value, options)
      I18n.l(value, format: :short)
    end
  end
end
