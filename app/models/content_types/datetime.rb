class ContentTypes::Datetime < ContentTypes::Value
  class << self
    def type
      :datetime
    end

    def cast(value)
      DateTime.parse(value) unless value.nil?
    end

    def serialize(value)
      value&.strftime("%FT%T")
    end

    def deserialize(value)
      value
    end

    def localized_string(value, options)
      I18n.l(cast(value), format: :default)
    end
  end
end
