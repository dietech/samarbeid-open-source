class Page < ApplicationRecord
  before_validation :set_slug
  validates :slug, :title, presence: true
  validates :slug, uniqueness: true

  def to_param
    slug
  end

  # for rails admin
  def name
    slug
  end

  private

  def set_slug
    self.slug = slug&.parameterize
  end
end
