class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :set_sentry_context

  rescue_from CanCan::AccessDenied do |exception|
    has_layout = exception.subject.to_s != "rails_admin"
    respond_to do |format|
      format.html { render template: "pages/not_authorized", status: 403, layout: has_layout }
      format.json { head :forbidden }
    end
  end

  private

  def set_sentry_context
    Sentry.set_user(id: current_user.id, email: current_user.email) if defined?(current_user) && current_user
    Sentry.set_extras(params: params.to_unsafe_h, url: request.url)
  end
end
