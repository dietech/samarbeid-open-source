class ApiController < ApplicationController
  include ApiResponse
  include ApiExceptionHandler
  check_authorization # Ensure all API Controller Actions are protected by authorization

  private

  def convert_null_to_nil(array_of_values)
    array_of_values.map do |value|
      next if value.eql?("null")
      value
    end
  end
end
