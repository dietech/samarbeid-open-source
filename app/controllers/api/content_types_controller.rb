class Api::ContentTypesController < ApiController
  skip_authorization_check

  def index
    @content_types = ContentType.types_array
  end
end
