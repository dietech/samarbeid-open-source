class Api::UsersController < ApiController
  load_and_authorize_resource

  def index
    page = params[:page]&.to_i || 1
    query = params[:query].blank? ? "*" : params[:query]
    tab_category = params[:tab_category] ||= "ALL"

    case (params[:order] ||= "name_asc")
    when "created_at_desc"
      order_attribute = :created_at
      order_sequence = :desc
    when "created_at_asc"
      order_attribute = :created_at
      order_sequence = :asc
    when "name_desc"
      order_attribute = :name
      order_sequence = :desc
    when "name_asc"
      order_attribute = :name
      order_sequence = :asc
    end

    additional_query_params = {
      where: {
        tab_categories: tab_category
      },
      order: {order_attribute => order_sequence},
      page: page, per_page: 10
    }

    @result = User.search_for_index(current_user, query, true, additional_query_params)
  end

  def list
    @users = User.filter_for_list(current_user, apply_filter_to: @users,
      query: params[:query],
      excluded_user_ids: params[:except].to_a,
      group_ids: params[:group_ids].to_a)
  end

  def show
  end

  def create
    @user = User.create(user_params)

    api_response_with(view: :show, errors: @user.errors)
  end

  def info
    @count_new_notifications = Notification.where(user: current_user, done_at: nil).count
    @count_open_user_assigned_tasks = Task.current_and_assigned_to(current_user).count
    @has_due_open_user_assigned_tasks = Task.current_and_assigned_to(current_user).with_due_in_past.exists?
  end

  private

  wrap_parameters User, include: User.attribute_names + [:password, :password_confirmation]
  def user_params
    params.require(:user).permit([:email, :firstname, :lastname, :password, :password_confirmation])
  end
end
