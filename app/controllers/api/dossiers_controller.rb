class Api::DossiersController < ApiController
  load_and_authorize_resource except: [:new, :create]
  before_action :check_for_deleted

  def index
    page = params[:page]&.to_i || 1
    query = params[:query].blank? ? "*" : params[:query]
    definition_ids = params[:definition_ids] ||= []

    case (params[:order] ||= "updated_at_desc")
    when "created_at_desc"
      order_attribute = :created_at
      order_sequence = :desc
    when "updated_at_desc"
      order_attribute = :updated_at
      order_sequence = :desc
    when "updated_at_asc"
      order_attribute = :updated_at
      order_sequence = :asc
    when "title_desc"
      order_attribute = :title
      order_sequence = :desc
    when "title_asc"
      order_attribute = :title
      order_sequence = :asc
    end

    additional_query_params = {
      where: {
        _and: [
          {_or: definition_ids.map { |id| {definition_id: id} }}
        ]
      },
      order: {order_attribute => order_sequence}
    }
    additional_query_params = additional_query_params.merge({page: page, per_page: 10}) unless params[:format].eql?("xlsx")

    @dossier_definitions = DossierDefinition.accessible_by(current_ability).order(name: :asc).to_a

    @result = Dossier.search_for_index(current_user, query, false, additional_query_params)

    if params[:format].eql?("xlsx")
      send_data Services::DossiersToTable.new(@result).to_excel
    end
  end

  def list
    definition_id = params[:definition_id]
    excluded_dossier_ids = params[:except].to_a
    query = params[:query].blank? ? "*" : params[:query]

    additional_query_params = {
      where: {
        id: {not: excluded_dossier_ids}
      },
      order: {title: :asc}
    }

    unless definition_id.nil?
      additional_query_params.deep_merge!({
        where: {
          definition_id: definition_id
        }
      })
    end

    @result = Dossier.search_for_list(current_user, query, additional_query_params)
  end

  def new
    dossier_definition = DossierDefinition.find(params.require(:dossier_definition_id))
    @dossier = Dossier.new(definition: dossier_definition)
    authorize!(:new, @dossier)
  end

  def create
    dossier_definition = DossierDefinition.find(params.require(:dossier_definition_id))
    authorize!(:create, Dossier.new(definition: dossier_definition))
    @dossier = Dossier.create(definition: dossier_definition, field_data: params[:field_data].to_unsafe_hash)
    Events::CreatedEvent.create!(subject: current_user, object: @dossier) if @dossier.valid?
    api_response_with(view: :create, errors: @dossier.errors)
  end

  def show
    set_reference_count
  end

  def update
    @dossier.set_field_value(params.require(:dossier_field_definition_id), params.fetch(:value))
    if @dossier.save
      set_reference_count
    end

    api_response_with(view: :show, errors: @dossier.errors)
  end

  def destroy
    @dossier.update(deleted: true)
    api_response_with(status: :ok, errors: @dossier.errors)
  end

  def show_references
    @workflows = referenced_workflows_for_current_user
  end

  private

  def check_for_deleted
    return unless @dossier
    raise RecordDeleted if @dossier.deleted
  end

  def referenced_workflows_for_current_user
    Workflow
      .not_deleted
      .where(id: Workflow.search_for_list(current_user, "*", {limit: nil}).map(&:id))
      .where(id: @dossier.referenced_in.map(&:id)).order(id: :desc)
  end

  def set_reference_count
    referenced_workflows = referenced_workflows_for_current_user
    @reference_count = referenced_workflows.count
  end
end
