class Api::DossierDefinitionsController < ApiController
  load_and_authorize_resource

  def index
    page = params[:page]&.to_i || 1
    query = params[:query].blank? ? "*" : params[:query]

    case (params[:order] ||= "name_asc")
    when "created_at_desc"
      order_attribute = :created_at
      order_sequence = :desc
    when "created_at_asc"
      order_attribute = :created_at
      order_sequence = :asc
    when "name_desc"
      order_attribute = :name
      order_sequence = :desc
    when "name_asc"
      order_attribute = :name
      order_sequence = :asc
    end

    additional_query_params = {
      order: {order_attribute => order_sequence},
      page: page, per_page: 10
    }

    # For UI-Admin Area we want to allow some users to see all Dossier Definitions - this currently is the easiest way.
    search_user = current_user.is_admin? ? User.new(groups: Group.all) : current_user

    @result = DossierDefinition.search_for_index(search_user, query, true, additional_query_params)
  end

  def list
    excluded_ids = params[:except].to_a
    included_ids = params[:included_ids].to_a
    limit = params[:max_result_count]&.to_i || 10
    query = params[:query].blank? ? "*" : params[:query]

    additional_query_params = {
      where: {
        _and: [
          excluded_ids.any? ? {id: {not: excluded_ids}} : nil,
          included_ids.any? ? {id: included_ids} : nil
        ]
      },
      order: {name: :asc},
      limit: limit
    }

    @dossier_definitions = DossierDefinition.search_for_list(current_user, query, additional_query_params)
  end

  def show
  end

  def create
    dossier_definition = Interactors::DossierDefinitionInteractor.create(create_params, current_user)
    api_response_with(object: {id: dossier_definition.id}, errors: dossier_definition.errors)
  end

  def update
    Interactors::DossierDefinitionInteractor.update(@dossier_definition, update_params, current_user)
    api_response_with(view: :show, errors: @dossier_definition.errors)
  end

  def destroy
    Interactors::DossierDefinitionInteractor.destroy(@dossier_definition)
    api_response_with(status: :ok, errors: @dossier_definition.errors)
  end

  private

  wrap_parameters DossierDefinition, include: DossierDefinition.attribute_names + [:group_ids]

  def create_params
    params.require(:dossier_definition).permit(:name, :description, group_ids: [])
  end

  def update_params
    params.require(:dossier_definition).permit(:name, :description, group_ids: [], title_fields: [], subtitle_fields: [])
  end
end
