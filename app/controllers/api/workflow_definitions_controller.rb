class Api::WorkflowDefinitionsController < ApiController
  load_and_authorize_resource

  def index
    page = params[:page]&.to_i || 1
    query = params[:query].blank? ? "*" : params[:query]

    case (params[:order] ||= "name_asc")
    when "created_at_desc"
      order_attribute = :created_at
      order_sequence = :desc
    when "created_at_asc"
      order_attribute = :created_at
      order_sequence = :asc
    when "name_desc"
      order_attribute = :name
      order_sequence = :desc
    when "name_asc"
      order_attribute = :name
      order_sequence = :asc
    end

    additional_query_params = {
      order: {order_attribute => order_sequence},
      page: page, per_page: 10
    }

    # For UI-Admin Area we want to allow some users to see all Workflow Definitions - this currently is the easiest way.
    search_user = current_user.is_admin? ? User.new(groups: Group.all) : current_user

    @result = WorkflowDefinition.search_for_index(search_user, query, true, additional_query_params)
  end

  def list
    excluded_ids = params[:except].to_a
    limit = params[:max_result_count]&.to_i || 10
    query = params[:query].blank? ? "*" : params[:query]

    additional_query_params = {
      where: {
        id: {not: excluded_ids}
      },
      order: {name: :asc},
      limit: limit
    }

    @workflow_definitions = WorkflowDefinition.search_for_index(current_user, query, false, additional_query_params)
  end

  def show
  end

  def create
    workflow_definition = Interactors::WorkflowDefinitionInteractor.create(workflow_definition_params, current_user)
    api_response_with(object: {id: workflow_definition.id}, errors: workflow_definition.errors)
  end

  def update
    Interactors::WorkflowDefinitionInteractor.update(@workflow_definition, workflow_definition_params, current_user)
    api_response_with(view: :show, errors: @workflow_definition.errors)
  end

  def destroy
    Interactors::WorkflowDefinitionInteractor.destroy(@workflow_definition, current_user)
    api_response_with(status: :ok, errors: @workflow_definition.errors)
  end

  private

  def workflow_definition_params
    params.permit(:name, :description, group_ids: [])
  end
end
