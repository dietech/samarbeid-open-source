json.array! data_fields do |data_field|
  json.partial! "/api/data_fields/data_field", data_field: data_field

  json.definition do
    json.infobox data_field[:definition].infobox
    json.task_item_id data_field[:definition].task_item_id
    json.task_item_count data_field[:definition].task_item_count
    json.block_count data_field[:definition].block_count
  end

  json.confirmedAt data_field[:confirmed_at]
  json.lastUpdated do
    json.at data_field[:last_updated]
    json.by do
      json.partial! "/api/users/user", user: data_field[:last_updated_by]
    end
  end
end
