json.id workflow_definition.id
json.name workflow_definition.name
json.description strip_tags(workflow_definition.description)
