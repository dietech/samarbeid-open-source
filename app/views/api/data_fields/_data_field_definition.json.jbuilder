json.id data_field_definition.id
json.type data_field_definition.content_type.to_s
json.required data_field_definition.required
json.options data_field_definition.options
json.name data_field_definition.name
