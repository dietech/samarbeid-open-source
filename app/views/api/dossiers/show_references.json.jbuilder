json.array! @workflows do |workflow|
  json.workflow do
    json.partial! "/api/workflows/workflow_minimal", workflow: workflow
  end

  json.dossierFields workflow.content_items.select { |ci| ci.content_type.name == ContentTypes::Dossier.name } do |content_item|
    dossiers = content_item.value
    dossiers = dossiers.to_a.compact.reject(&:deleted?)

    if dossiers.any?
      json.id content_item.id
      json.label content_item.label

      json.value do
        json.partial! "api/dossiers/dossier_list_item", collection: dossiers, as: :dossier
      end
    end
  end
end
