json.id group.id
json.name group.name
json.userCount group.users.count
json.workflowDefinitionCount group.workflow_definitions.count
