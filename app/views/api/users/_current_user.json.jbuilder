json.partial! "/api/users/user", user: user

unless user.nil?
  json.isAdmin user.is_admin?
  json.isSuperAdmin user.super_admin?
  json.referenceLabel user.reference_label
end
