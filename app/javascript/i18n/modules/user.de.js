export default {
  actions: {
    create: 'Nutzer erstellen'
  },
  detailActions: {
    deactivate: 'Nutzer deaktivieren',
    activate: 'Nutzer aktivieren'
  },
  createDialog: {
    title: 'Neuen Nutzer erstellen',
    buttonOk: 'Nutzer erstellen',
    fields: {
      email: 'E-Mail',
      firstname: 'Vorname',
      lastname: 'Nachname',
      password: 'Passwort',
      password_confirmation: 'Passwort wiederholen'
    }
  }
}
