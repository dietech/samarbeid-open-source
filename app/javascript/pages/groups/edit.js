import Page from '../page'
import GroupEditContent from './edit-content'

export default {
  name: 'GroupEditPage',
  mixins: [Page],
  props: {
    id: {
      type: Number,
      required: true
    }
  },
  computed: {
    pageTitleParts () {
      return [...(this.value ? [this.value.name] : []), 'Gruppen']
    },
    pageContentComponents () {
      return GroupEditContent
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.groups.show(this.id)
    }
  }
}
