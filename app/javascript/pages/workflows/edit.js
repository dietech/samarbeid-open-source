import Page from '../page'
import EditWorkflowContent from './edit-workflow-content'
import EditTaskContent from './edit-task-content'
import EditSidebarLeft from './edit-sidebar-left'
import EditWorkflowSidebarRight from './edit-workflow-sidebar-right'
import EditTaskSidebarRight from './edit-task-sidebar-right'
import Workflow from 'mixins/models/workflow'

export default {
  name: 'WorkflowEditPage',
  mixins: [Page, Workflow],
  props: {
    id: {
      type: Number,
      required: true
    },
    type: {
      type: String,
      required: true
    }
  },
  data () {
    return {
      designMode: false
    }
  },
  computed: {
    pageTitleParts () {
      if (this.type === 'task') {
        return [...(this.value && this.workflowGetTaskWithIdFor(this.value.workflow, this.id) ? [this.workflowGetTaskWithIdFor(this.value.workflow, this.id).referenceLabel] : []), 'Aufgaben']
      } else {
        return [...(this.value && this.value.workflow ? [this.value.workflow.referenceLabel] : []), 'Maßnahmen']
      }
    },
    pageContentComponents () {
      return (this.type === 'workflow') ? EditWorkflowContent : EditTaskContent
    },
    pageSidebarLeftComponents () {
      return EditSidebarLeft
    },
    pageSidebarRightComponents () {
      return (this.type === 'workflow') ? EditWorkflowSidebarRight : EditTaskSidebarRight
    },
    pageSidebarLeftFloating () {
      return true
    },
    pageComponentProps () {
      return { designMode: this.designMode }
    }
  },
  methods: {
    pageClass () {
      return `${this.type}-edit-page`
    },
    onPropsChanged () {
      if (this.value && this.value.workflow) {
        if (this.type === 'workflow' && this.value.workflow.id === this.id) {
          this.value.task = null
          return
        } else if (this.type === 'task') {
          this.value.task = this.workflowGetTaskWithIdFor(this.value.workflow, this.id)
          if (this.value.task) return
        }
      }

      this.value = null
      this.initPageRequestParameter()
      this.fetchPageData()
    },
    onRequestSuccess (data) {
      this.value = {
        workflow: data,
        task: (this.type === 'task') ? this.workflowGetTaskWithIdFor(data, this.id) : null
      }

      if (!this.workflowHasAvailableActionFor(this.value.workflow, 'design')) {
        this.designMode = false
      }
    },
    initPageRequestUrl () {
      return (this.type === 'workflow') ? this.$apiEndpoints.workflows.show(this.id) : this.$apiEndpoints.tasks.show(this.id)
    },
    onPropUpdated (prop, value, info) {
      if (prop === 'designMode') {
        this.designMode = value
      }
    },
    onValueChanged () {
      if (!this.workflowHasAvailableActionFor(this.value.workflow, 'design')) {
        this.designMode = false
      }
    }
  }
}
