import Page from '../page'
import WorkflowDefinitionEditContent from './edit-content'

export default {
  name: 'WorkflowDefinitionEditPage',
  mixins: [Page],
  props: {
    id: {
      type: Number,
      required: true
    }
  },
  computed: {
    pageTitleParts () {
      return [...(this.value ? [this.value.name] : []), 'Maßnahmevorlagen']
    },
    pageContentComponents () {
      return WorkflowDefinitionEditContent
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.workflowDefinitions.show(this.id)
    }
  }
}
