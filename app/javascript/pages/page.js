import kebabCase from 'lodash/kebabCase'
import join from 'lodash/join'
import castArray from 'lodash/castArray'
import forEach from 'lodash/forEach'
import has from 'lodash/has'
import assign from 'lodash/assign'
import Request from 'api/request'
import Requestable from 'mixins/requestable'
import PageContent from './page-content'
import SidebarLeft from './sidebar-left'
import SidebarRight from './sidebar-right'

export const PageContentable = {
  props: {
    value: {
      type: Object,
      default: null
    },
    pageRequestLoading: Boolean,
    pageRequestHasError: Boolean,
    pageRequestParameter: {
      type: Object,
      required: true
    },
    hasLeftSidebar: {
      type: Boolean,
      required: true
    },
    hasRightSidebar: {
      type: Boolean,
      required: true
    }
  },
  methods: {
    fetchPageData (pageRequestParameter) {
      this.$emit('fetchPageData', pageRequestParameter)
    }
  }
}

export default {
  name: 'Page',
  mixins: [Requestable],
  data () {
    return {
      value: null,
      pageRequestParameter: {
        method: null,
        url: null,
        params: null,
        data: null,
        useCancelToken: null
      },
      sidebarLeftOpen: undefined,
      sidebarRightOpen: undefined
    }
  },
  computed: {
    pageTitleParts () {
      return null
    },
    pageContentComponents () {
      return null
    },
    pageSidebarLeftComponents () {
      return null
    },
    pageSidebarRightComponents () {
      return null
    },
    pageSidebarLeftFloating () {
      return false
    },
    pageComponentProps () {
      return {}
    },
    pagePropsToWatch () {
      return this.$props
    }
  },
  watch: {
    pagePropsToWatch: {
      handler () {
        this.onPropsChanged()
      },
      deep: true,
      immediate: true
    },
    pageTitleParts: {
      handler () {
        this.setPageTitle()
      },
      immediate: true
    }
  },
  methods: {
    onPropsChanged () {
      this.onBeforePropsChanged()
      this.initPageRequestParameter()
      this.fetchPageData()
    },
    onBeforePropsChanged () {
    },
    onValueChanged () {
    },
    initPageRequestMethod () {
      return Request.GET
    },
    initPageRequestUrl () {
      return null
    },
    initPageRequestParams () {
      return null
    },
    initPageRequestData () {
      return null
    },
    initPageRequestUseCancelToken () {
      return true
    },
    initPageRequestParameter () {
      this.pageRequestParameter.method = this.initPageRequestMethod()
      this.pageRequestParameter.url = this.initPageRequestUrl()
      this.pageRequestParameter.params = this.initPageRequestParams()
      this.pageRequestParameter.data = this.initPageRequestData()
      this.pageRequestParameter.useCancelToken = this.initPageRequestUseCancelToken()
    },
    fetchPageData () {
      if (this.hasError) this.resetRequestable()
      if (this.pageRequestParameter.method && this.pageRequestParameter.url) {
        this.request(
          { method: this.pageRequestParameter.method, url: this.pageRequestParameter.url },
          this.pageRequestParameter.params,
          this.pageRequestParameter.data,
          this.pageRequestParameter.useCancelToken,
          true)
      }
    },
    onRequestSuccess (data) {
      this.value = data
    },
    genPageElements () {
      const elements = []

      if (this.pageSidebarLeftComponents || this.pageSidebarRightComponents) {
        elements.push(
          this.$createElement(SidebarLeft, {
            props: {
              value: this.sidebarLeftOpen,
              floating: !this.pageSidebarLeftComponents || this.pageSidebarLeftFloating
            },
            on: {
              input: (value) => {
                this.sidebarLeftOpen = value
              }
            }
          }, this.genPageComponents(this.pageSidebarLeftComponents))
        )
      }

      elements.push(
        this.$createElement(PageContent, {}, this.genPageComponents(this.pageContentComponents))
      )

      if (this.pageSidebarRightComponents) {
        elements.push(
          this.$createElement(SidebarRight, {
            props: {
              value: this.sidebarRightOpen
            },
            on: {
              input: (value) => {
                this.sidebarRightOpen = value
              }
            }
          }, this.genPageComponents(this.pageSidebarRightComponents))
        )
      }

      return elements
    },
    genPageComponents (components) {
      const elements = []

      if (this.error && [403, 404, 410].includes(this.error.code)) return elements

      forEach(castArray(components), (component) => {
        if (component) {
          const props = assign({
            value: this.value,
            pageRequestLoading: this.requestableLoading,
            pageRequestHasError: this.hasError,
            pageRequestParameter: this.pageRequestParameter,
            hasLeftSidebar: !!this.pageSidebarLeftComponents,
            hasRightSidebar: !!this.pageSidebarRightComponents
          }, this.pageComponentProps)

          elements.push(this.$createElement(component, {
            props: props,
            on: {
              input: (value) => {
                this.value = value
                this.onValueChanged()
              },
              fetchPageData: (value) => {
                if (has(value, 'method')) this.pageRequestParameter.method = value.method
                if (has(value, 'url')) this.pageRequestParameter.url = value.url
                if (has(value, 'params')) this.pageRequestParameter.params = value.params
                if (has(value, 'data')) this.pageRequestParameter.data = value.data
                if (has(value, 'useCancelToken')) this.pageRequestParameter.useCancelToken = value.useCancelToken
                this.fetchPageData()
              },
              'update:prop': ({ prop, value, info }) => {
                this.onPropUpdated(prop, value, info)
              },
              'open-sidebar': (type) => {
                switch (type) {
                  case 'left':
                    this.sidebarLeftOpen = true
                    break
                  case 'right':
                    this.sidebarRightOpen = true
                    break
                }
              }
            }
          }))
        }
      })

      return elements
    },
    setPageTitle () {
      const titleParts = this.pageTitleParts
      if (titleParts && titleParts.length) {
        document.title = `${join(titleParts, ' • ')} • ${this.$t('general.appName')}`
      } else {
        document.title = this.$t('general.appName')
      }
    },
    onPropUpdated (prop, value, info) {
    },
    pageClass () {
      return kebabCase(this.$options.name)
    }
  },
  render: function (createElement) {
    return createElement('div', {
      staticClass: this.pageClass()
    }, [this.genPageElements()])
  }
}
