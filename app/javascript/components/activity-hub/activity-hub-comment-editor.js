// Styles
import 'controls/richtextarea/richtextarea.sass'

// Extensions
import Richtextarea from 'controls/richtextarea/richtextarea'

// Components
import { TiptapVuetify } from 'tiptap-vuetify'

// Utilities
import mixins from 'vuetify/lib/util/mixins'
const baseMixins = mixins(Richtextarea)

/* @vue/component */
export default baseMixins.extend({
  name: 'ActivityHubCommentEditor',
  methods: {
    genContent () {
      return [this.$createElement(TiptapVuetify, {
        props: {
          value: this.value,
          extensions: this.extensions,
          placeholder: this.placeholder,
          cardProps: { flat: false, loading: this.loading, disabled: this.disabled },
          disabled: this.disabled,
          nativeExtensions: [this.mentionableExtension]
        },
        on: {
          input: this.onEditorInput
        },
        attrs: {
          ...this.attrs$,
          id: this.computedId
        },
        ref: 'tiptapVuetify'
      }),
      this.errorMessages
        ? this.$createElement('div', {
          class: 'text-caption error--text mt-2'
        }, this.errorMessages)
        : this.$createElement('div', {
          class: 'text-caption secondary--text text--lighten-4 mt-2'
        }, this.hint),
      this.genMentionMenu()
      ]
    }
  },

  render (h) {
    return h('div', {
      class: 'activity-hub-comment-editor'
    }, this.genContent())
  }
})
