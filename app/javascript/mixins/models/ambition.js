import { stateColors } from 'helpers/definitions'
import Workflow from 'mixins/models/workflow'
import reduce from 'lodash/reduce'

export default {
  methods: {
    ambitionStateTextFor (ambition) {
      if (ambition.closed) {
        return this.$t('ambition.state.closed')
      } else {
        return this.$t('ambition.state.open')
      }
    },
    ambitionRunningWorkflowCountTextFor (ambition) {
      return this.$tc('ambition.openWorkflowsCount', ambition.runningWorkflowCount)
    },
    ambitionSubtitleElementsFor (ambition) {
      const elements = [`!${ambition.id}`, this.ambitionStateTextFor(ambition)]
      if (!ambition.closed) {
        elements.push(this.ambitionRunningWorkflowCountTextFor(ambition))
      }

      return elements
    },
    ambitionBusinessObjectFor (ambition) {
      return {
        id: ambition.id,
        title: ambition.title,
        subtitleElements: this.ambitionSubtitleElementsFor(ambition),
        avatar: null,
        type: 'Ziel',
        href: ''
      }
    },
    ambitionNoAccessFor (ambition) {
      return ambition.noAccess === true
    }
  },
  computed: {
    ambitionBreadcrumbs () {
      return [
        { text: 'Ziele', to: { name: 'ambitions' } },
        { text: `!${this.ambition.id}` }
      ]
    },
    ambitionStateText () {
      return this.ambitionStateTextFor(this.ambition)
    },
    ambitionStateColor () {
      if (this.ambition.closed) {
        return stateColors.completed
      } else {
        return stateColors.started
      }
    },
    ambitionStateUpdatedAtDate () {
      return new Date(this.ambition.stateUpdatedAt)
    },
    ambitionIdentifier () {
      return `!${this.ambition.id}`
    },
    ambitionRunningWorkflowCountText () {
      return this.ambitionRunningWorkflowCountTextFor(this.ambition)
    },
    ambitionRunningWorkflowWithAccessCount () {
      return reduce(this.ambition.workflows, function (result, value) {
        return result + ((!Workflow.methods.workflowNoAccessFor(value) && Workflow.methods.workflowIsActiveFor(value)) ? 1 : 0)
      }, 0)
    },
    ambitionHasNoAccessWorkflows () {
      return reduce(this.ambition.workflows, function (result, value) {
        return result || Workflow.methods.workflowNoAccessFor(value)
      }, false)
    }
  }
}

// This could be an option if we need to namespace computed properties to use them with multiple objects
/*
export default function(refName = "ambition", namespace = "") {
    return {
        computed: {
            [`${refName}Breadcrumbs`]() {
                return [`Ziel !${this[refName].id}`];
            },
            breadcrumbs() {
                return [`Ziel !${this[refName].id}`];
            }
        }
    }
} */
