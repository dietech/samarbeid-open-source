# Based on production defaults
require Rails.root.join("config/environments/production")

Rails.application.configure do
  # To see more set to :debug temporarily
  # config.log_level = :debug
  config.log_level = :info

  # Email-Setup
  config.action_mailer.delivery_method = :test # No Mails on staging!

  # To make it easier running a "almost" production environment locally we must change the settings below
  # config.force_ssl = false # Otherwise we have to configure local puma with certificates etc.
  # config.public_file_server.enabled = true # No need to precompile assets AND serve them from root (public folder)
end
