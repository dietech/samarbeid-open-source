unless File.writable?(Rails.root.join("storage"))
  Sentry.capture_message "The directory /storage must be writable for file uploads to work"
  raise "The directory /storage must be writable for file uploads to work"
end
