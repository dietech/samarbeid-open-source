require "test_helper"

class Services::DueTasksTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  test "find_unprocessed should only find those not yet processed" do
    task_one, task_two = Task.with_due_in_past.limit(2)
    task_one.update!(due_processed: true)
    task_two.update!(due_processed: false)

    unprocessed = Services::DueTasks.find_unprocessed
    refute unprocessed.include?(task_one)
    assert unprocessed.include?(task_two)

    Services::DueTasks.find_and_process

    unprocessed = Services::DueTasks.find_unprocessed
    refute unprocessed.include?(task_one)
    refute unprocessed.include?(task_two)
  end

  test "find_and_process should do everything necessary" do
    task = Task.with_due_in_past.first
    task.update!(due_processed: false)

    reindex_job_for_task = {job: Searchkick::ReindexV2Job, args: [task.class.name, task.id.to_s, nil, {routing: nil}]}

    assert_enqueued_with(**reindex_job_for_task) do
      Services::DueTasks.find_and_process
    end
    assert task.reload.due_processed
  end

  test "setting due_at date for tasks should reset processed state only if the task may still become due" do
    task = Task.with_due_in_past.first
    task.update!(due_processed: true)

    task.update!(due_at: 1.day.ago)
    assert task.reload.due_processed

    task.update!(due_at: 1.day.from_now)
    refute task.reload.due_processed # will become due tomorrow
  end
end
