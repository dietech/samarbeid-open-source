require "test_helper"

class WheneverTest < ActiveSupport::TestCase
  test "there exist runner entries" do
    schedule = Whenever::Test::Schedule.new(file: "config/schedule.rb")

    assert schedule.jobs[:runner].count > 0
  end

  test "all runner entries may be called without causing exceptions" do
    schedule = Whenever::Test::Schedule.new(file: "config/schedule.rb")

    schedule.jobs[:runner].each do |job|
      assert_nothing_raised { instance_eval job[:task] }
    end
  end
end
