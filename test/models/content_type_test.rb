require "test_helper"

class ContentTypeTest < ActiveSupport::TestCase
  test "content type 'decimal' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::Decimal, 99.99, "99.99")
    test_content_type(ContentTypes::Decimal, "99.99", "99.99")
    test_content_type(ContentTypes::Decimal, BigDecimal("99.99"), "99.99")
    test_content_type(ContentTypes::Decimal, 99, "99.0")
  end

  test "content type 'integer' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::Integer, 99, 99)
    test_content_type(ContentTypes::Integer, "99", 99)
  end

  test "content type 'boolean' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::Boolean, true, true)
    test_content_type(ContentTypes::Boolean, false, false)
    test_content_type(ContentTypes::Boolean, "true", true)
    test_content_type(ContentTypes::Boolean, "false", false)
  end

  test "content type 'date' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::Date, "2021-03-01", "2021-03-01")
    test_content_type(ContentTypes::Date, Date.parse("2021-03-01"), "2021-03-01")
  end

  test "content type 'time' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::Time, "14:45", "14:45")
    test_content_type(ContentTypes::Time, Time.parse("14:45"), "14:45")
  end

  test "content type 'datetime' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::Datetime, "2021-04-27T13:25:00", "2021-04-27T13:25:00")
  end

  test "content type 'dossier' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::Dossier, Dossier.find(1), ["gid://samarbeid/Dossier/1"])
    test_content_type(ContentTypes::Dossier, Dossier.find(1, 2), ["gid://samarbeid/Dossier/1", "gid://samarbeid/Dossier/2"])
  end

  test "content type 'file' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::File, UploadedFile.find(4), ["gid://samarbeid/UploadedFile/4"])
    test_content_type(ContentTypes::File, UploadedFile.find(4, 7), ["gid://samarbeid/UploadedFile/4", "gid://samarbeid/UploadedFile/7"])
  end

  private

  def test_content_type(content_type, value_in, value_stored_in_db)
    value_casted = content_type.cast(value_in)
    value_serialized = content_type.serialize(value_casted)
    value_deserialized = content_type.deserialize(value_serialized)

    assert_equal value_stored_in_db, value_serialized, "Wrong serialized value of content type '#{content_type}' #{caller_locations(1..1)&.map { |loc| "#{loc.path}:#{loc.lineno}" }}"
    assert_equal value_casted, value_deserialized, "Wrong deserialized value of content type '#{content_type}' #{caller_locations(1..1)&.map { |loc| "#{loc.path}:#{loc.lineno}" }}"
  end
end
