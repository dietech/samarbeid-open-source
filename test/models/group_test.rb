require "test_helper"

class GroupTest < ActiveSupport::TestCase
  test "groups may have several workflow definitions associated but each only once" do
    group = Group.find_by!(name: "tomsy")
    assert_nothing_raised do
      group.workflow_definitions = WorkflowDefinition.all.to_a
    end

    assert_raises ActiveRecord::RecordNotUnique do
      group.workflow_definitions << WorkflowDefinition.first
    end
  end

  test "groups may have several dossier definitions associated but each only once" do
    group = Group.find_by!(name: "tomsy")
    assert_nothing_raised do
      group.dossier_definitions = DossierDefinition.all.to_a
    end

    assert_raises ActiveRecord::RecordNotUnique do
      group.dossier_definitions << DossierDefinition.first
    end
  end
end
