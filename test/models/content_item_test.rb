require "test_helper"

class ContentItemTest < ActiveSupport::TestCase
  def setup
    workflow = WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch").create_workflow!

    @new_task = workflow.items.first
    @new_content_item = @new_task.task_items.map(&:content_item).find { |ci| ci.content_type == ContentTypes::Email }
  end

  test "content item should be undefined if value is changed" do
    content_item = ContentItem.where(content_type: ContentTypes::String.to_s).confirmed.first
    content_item.value = "#{content_item.value} let's change it a littel."
    assert content_item.undefined?

    content_item.save!
    assert content_item.reload.undefined?
  end

  test "content items should by default be undefined" do
    assert @new_content_item.undefined?
  end

  test "content items may be confirmed" do
    @new_content_item.confirm!
    assert @new_content_item.reload.confirmed?
  end

  test "empty content items should be confirmed when task completes" do
    start_and_complete(@new_task)

    assert @new_content_item.reload.confirmed?
  end

  test "content items with value should be confirmed when task completes" do
    @new_content_item.value = "some-email@example.com"
    @new_content_item.save!

    assert @new_content_item.undefined?
    start_and_complete(@new_task)

    assert @new_content_item.reload.confirmed?
  end

  test "a content_items confirmed_at is set/unset" do
    Timecop.freeze do
      @new_content_item.confirm!
      assert_equal Time.now, @new_content_item.confirmed_at
    end

    @new_content_item.change!
    refute @new_content_item.confirmed_at
  end

  test "a confirmed content_item validates values correctly confirmed or not" do
    @new_content_item.value = nil
    assert @new_content_item.valid?

    @new_content_item.value = ""
    assert @new_content_item.valid?

    @new_content_item.value = "not a email"

    refute @new_content_item.valid?
    @new_content_item.value = "a-email@example.org"
    assert @new_content_item.valid?

    @new_content_item.confirm!

    @new_content_item.value = "not a email"
    refute @new_content_item.valid?

    @new_content_item.value = "another-email@example.org"
    assert @new_content_item.valid?
  end

  private

  def start_and_complete(task)
    task.start! if task.may_start?
    task.complete!
  end
end
