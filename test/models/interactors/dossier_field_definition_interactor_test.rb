require "test_helper"

class Interactors::DossierFieldDefinitionInteractorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    @dossier_field_definition = @dossier_definition.fields.second
  end

  test "#create should create a dossier field definition at the last position" do
    new_dossier_field_definition = nil
    last_position = @dossier_definition.fields.max_by(&:position)&.position

    assert_difference -> { DossierFieldDefinition.count } do
      new_dossier_field_definition = Interactors::DossierFieldDefinitionInteractor.create(
        @dossier_definition,
        {name: "test dossier definition name", content_type: "text"}
      )
    end

    assert_operator new_dossier_field_definition.position, :>, last_position
    assert_includes @dossier_definition.fields, new_dossier_field_definition
  end

  test "#update should update dossier field definition" do
    Interactors::DossierFieldDefinitionInteractor.update(@dossier_field_definition, {name: "updated name"})

    assert_equal "updated name", @dossier_field_definition.name
  end

  test "#destroy should delete a dossier field definition, remove field from title and subtile of dossier definition and remove it from data of dossiers" do
    @dossier_definition.title_fields |= [@dossier_field_definition.id]
    @dossier_definition.subtitle_fields |= [@dossier_field_definition.id]
    @dossier_definition.save!

    dossier = @dossier_definition.dossiers.first
    assert_includes dossier.data, @dossier_field_definition.id.to_s

    assert_difference -> { DossierFieldDefinition.count }, -1 do
      Interactors::DossierFieldDefinitionInteractor.destroy(@dossier_field_definition)
    end

    assert_empty @dossier_field_definition.errors
    refute_includes @dossier_definition.title_fields, @dossier_field_definition.id
    refute_includes @dossier_definition.subtitle_fields, @dossier_field_definition.id
    refute_includes dossier.reload.data, @dossier_field_definition.id.to_s
  end

  test "#mode should reorder the fields of a dossier definition by the attribute position" do
    # move from index 1 to index 2
    field_ids_order_old = DossierFieldDefinition.where(definition: @dossier_definition).order(:position).map(&:id)
    fields_count = field_ids_order_old.length
    assert_operator fields_count, :>=, 4
    assert_equal @dossier_field_definition.id, field_ids_order_old[1]
    Interactors::DossierFieldDefinitionInteractor.move(@dossier_field_definition, 2)
    field_ids_order_act = @dossier_definition.fields.reload.map(&:id)
    field_ids_order_exp = field_ids_order_old.dup
    field_ids_order_exp[1], field_ids_order_exp[2] = field_ids_order_exp[2], field_ids_order_exp[1]
    assert_equal field_ids_order_exp, field_ids_order_act

    # move from index 2 to first position
    field_ids_order_old = field_ids_order_act
    Interactors::DossierFieldDefinitionInteractor.move(@dossier_field_definition, 0)
    field_ids_order_act = @dossier_definition.fields.reload.map(&:id)
    field_ids_order_exp = field_ids_order_old.dup
    field_ids_order_exp[0], field_ids_order_exp[1], field_ids_order_exp[2] = field_ids_order_exp[2], field_ids_order_exp[0], field_ids_order_exp[1]
    assert_equal field_ids_order_exp, field_ids_order_act

    # move from index 0 to last position
    field_ids_order_old = field_ids_order_act
    Interactors::DossierFieldDefinitionInteractor.move(@dossier_field_definition, fields_count)
    field_ids_order_act = @dossier_definition.fields.reload.map(&:id)
    field_ids_order_exp = field_ids_order_old.dup
    first_id = field_ids_order_exp.shift
    field_ids_order_exp.push(first_id)
    assert_equal field_ids_order_exp, field_ids_order_act
  end
end
