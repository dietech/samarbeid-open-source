require "test_helper"

class Interactors::CommentInteractorTest < ActiveSupport::TestCase
  def setup
    @current_user = User.find_by!(email: "admin@example.org")
    @any_task = Task.first
  end

  test "#create should create comment and event and add to contributors if necessary" do
    @any_task.update!(contributors: [])

    assert_difference -> { Events::CommentedEvent.count } do
      assert_difference -> { Comment.count } do
        Interactors::CommentInteractor.create("My own comment.", @any_task, @current_user)
      end
    end

    assert @any_task.contributors.include?(@current_user)
  end

  test "#create should add mentioned users if present" do
    any_user = User.first
    Interactors::CommentInteractor.create("mention a user <mention m-id=\"#{any_user.id}\" m-type=\"user\">#{any_user.mention_label}</mention>", @any_task, @current_user)
    created_event = Events::CommentedEvent.for_object(@any_task).last
    assert_equal [any_user], created_event.mentioned_users
  end

  test "it should be possible to create a comment for a workflow, task, ambition, workflow_definition and dossier" do
    object = Task.first
    assert_difference -> { Events::CommentedEvent.count } do
      assert_difference -> { object.comments.count } do
        Interactors::CommentInteractor.create("My own comment.", object, @current_user)
      end
    end

    object = Workflow.first
    assert_difference -> { Events::CommentedEvent.count } do
      assert_difference -> { object.comments.count } do
        Interactors::CommentInteractor.create("My own comment.", object, @current_user)
      end
    end

    object = Ambition.first
    assert_difference -> { Events::CommentedEvent.count } do
      assert_difference -> { object.comments.count } do
        Interactors::CommentInteractor.create("My own comment.", object, @current_user)
      end
    end

    object = WorkflowDefinition.first
    assert_difference -> { Events::CommentedEvent.count } do
      assert_difference -> { object.comments.count } do
        Interactors::CommentInteractor.create("My own comment.", object, @current_user)
      end
    end

    object = Dossier.first
    assert_difference -> { Events::CommentedEvent.count } do
      assert_difference -> { object.comments.count } do
        Interactors::CommentInteractor.create("My own comment.", object, @current_user)
      end
    end
  end
end
