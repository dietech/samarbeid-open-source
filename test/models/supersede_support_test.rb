require "test_helper"

class SupersedeSupportTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    workflow_definition = WorkflowDefinition.find_by(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
    @workflow = workflow_definition.create_workflow!
    assert @workflow.persisted?
    @task1 = @workflow.all_tasks.first
    assert_equal 1, @task1.events.count

    @user = User.first
    @informed_user = User.find_by!(email: "dsb-boss@example.org")
    @active_user = User.find_by!(email: "dsb@example.org")

    Interactors::WorkflowInteractor.update_assignee(@workflow, @informed_user, @user)
  end

  test "creating events supersedes specific existing ones in same object" do
    assert_equal 1, @workflow.events.count
    assert_nil @task1.events.first.superseder
    Interactors::TaskInteractor.start(@task1, @active_user)
    assert_not_nil @task1.events.first.superseder
    assert_equal @task1.events.find_by(type: "Events::StartedEvent"), @task1.events.first.superseder

    Interactors::TaskInteractor.update_assignee(@task1, @user, @active_user)
    assignment_events = @task1.events.where(type: "Events::AssignedEvent")
    assert_equal 2, assignment_events.count
    assert_nil assignment_events.last.superseder
    assert_equal assignment_events.last, assignment_events.first.superseder

    Interactors::TaskInteractor.update_assignee(@task1, nil, @active_user)
    assignment_events = @task1.events.where(type: ["Events::AssignedEvent", "Events::UnassignedEvent"])
    assert_equal 3, assignment_events.count
    assert_nil assignment_events.last.superseder
    assert_equal assignment_events.second, assignment_events.first.superseder
    assert_equal assignment_events.third, assignment_events.second.superseder
  end

  test "completing a workflow supersedes completion events of tasks" do
    assert_equal 1, @workflow.events.count
    assert_nil @task1.events.first.superseder
    Interactors::TaskInteractor.skip(@task1, @active_user)
    assert_not_nil @task1.events.first.superseder

    skipped_event = @task1.events.find_by(type: "Events::SkippedEvent")
    assert_equal skipped_event, @task1.events.first.superseder
    assert_nil skipped_event.superseder

    decision = @workflow.content_items.find_by(label: "Soll die Anzeige gelöscht werden?")
    decision.update(value: true)
    decision_task = @workflow.direct_tasks.second
    Interactors::TaskInteractor.complete(decision_task, @active_user)

    @workflow.all_tasks.each do |task|
      Interactors::TaskInteractor.skip(task, @active_user)
    end

    Interactors::WorkflowInteractor.complete(@workflow, @user)
    assert_equal 2, @workflow.events.count

    assert_not_nil skipped_event.reload.superseder
    workflow_completed_event = @workflow.events.find_by(type: "Events::CompletedEvent")
    assert_equal workflow_completed_event, skipped_event.superseder

    @workflow.all_tasks.map(&:events).flatten.filter { ["Events::CompletedEvent", "Events::SkippedEvent"].include?(_1.type)}.each do |event|
      assert_equal workflow_completed_event, event.superseder
    end
  end

  test "commenting an object supersedes previous comments" do
    comment1 = Interactors::CommentInteractor.create("First", @workflow, @active_user)
    comment2 = Interactors::CommentInteractor.create("Second", @workflow, @informed_user)
    comment3 = Interactors::CommentInteractor.create("Third", @workflow, @user)
    comment4 = Interactors::CommentInteractor.create("Fourth", @workflow, @active_user)

    assert_equal 4, @workflow.comments.count
    [comment1, comment2, comment3, comment4].each do |comment|
      assert_equal 1, comment.events.count
    end
    assert_equal comment2.events.first, comment1.events.first.superseder
    assert_equal comment3.events.first, comment2.events.first.superseder
    assert_equal comment4.events.first, comment3.events.first.superseder
    assert_nil comment4.events.first.superseder
  end

  test "superseding an event archives its notifications" do
    assert_equal 1, @workflow.events.count
    Interactors::TaskInteractor.update_assignee(@task1, @user, @active_user)
    event = @task1.events.last
    assert_nil event.superseder
    assert_equal 1, event.notifications.count
    assert_equal @user, event.notifications.first.user
    refute event.notifications.first.done?

    Interactors::TaskInteractor.update_assignee(@task1, @informed_user, @active_user)
    assert_not_nil event.reload.superseder
    assert event.notifications.first.done?
    event2 = @task1.events.last
    assert_equal 2, event2.notifications.count
    assert_not_nil event2.notifications.find_by(user: @user)
    assert_not_nil event2.notifications.find_by(user: @informed_user)
  end

  test "superseding an event doesn't archive marked notifications" do
    assert_equal 1, @workflow.events.count
    Interactors::TaskInteractor.update_assignee(@task1, @user, @active_user)
    event = @task1.events.last
    notification = event.notifications.first
    refute notification.done?

    notification.update(bookmarked_at: Time.now)

    Interactors::TaskInteractor.update_assignee(@task1, @informed_user, @active_user)
    assert_not_nil event.reload.superseder
    refute notification.reload.done?
  end

  test "superseding does not archive notifications for users who don't receive the new notification" do
    # https://gitlab.com/dietech/samarbeid/-/issues/877#note_803226528
    Interactors::TaskInteractor.update_assignee(@task1, @user, @active_user)

    assert_difference -> { @informed_user.notifications.count } do
      Interactors::TaskInteractor.skip(@task1, @active_user)
    end
    notification = @informed_user.notifications.order(:id).last
    refute notification.event.obsolete?
    refute notification.done?

    assert_no_difference -> { @informed_user.notifications.count } do
      Interactors::TaskInteractor.reopen(@task1, @active_user)
    end

    assert notification.event.reload.obsolete?
    refute notification.reload.done?
  end

  test "superseding does (currently not) archive notifications of transitive supersede chains" do
    # The behaviour tested here might be subject to change in the future.
    # If so, the last line needs to be changed from to refute to assert.

    # starting like previous test, but then superseding the skip event
    Interactors::TaskInteractor.update_assignee(@task1, @user, @active_user)

    assert_difference -> { @informed_user.notifications.count } do
      Interactors::TaskInteractor.skip(@task1, @active_user)
    end
    notification = @informed_user.notifications.order(:id).last
    refute notification.event.obsolete?
    refute notification.done?

    assert_no_difference -> { @informed_user.notifications.count } do
      Interactors::TaskInteractor.reopen(@task1, @active_user)
    end

    assert notification.event.reload.obsolete?
    refute notification.reload.done?

    # now supersede the skip event by a newer skip event
    assert_difference -> { @informed_user.notifications.count } do
      Interactors::TaskInteractor.skip(@task1, @active_user)
    end

    # we're currently NOT archiving the notification because its event was not touched again, only its superseder
    refute notification.reload.done?
  end

  test "writing a comment marks all comment notifications on same object archived for the subject who writes the comment" do
    comment1 = Interactors::CommentInteractor.create("First", @workflow, @active_user)
    assert_equal 2, comment1.events.first.notifications.count
    comment1.events.first.notifications.each do |notification|
      # not superseded yet, no notification is archived
      refute notification.done?
    end

    comment2 = Interactors::CommentInteractor.create("Second", @workflow, @informed_user)
    assert_equal 2, comment2.events.first.notifications.count
    # comment1 is superseded by comment2, which was written by the @informed_user, so his notification is archived
    assert comment1.events.first.notifications.find_by(user: @informed_user).done?
    # @user received a notification about comment2, but their notification about comment 1 is NOT archived
    refute comment1.events.first.notifications.find_by(user: @user).done?

    comment3 = Interactors::CommentInteractor.create("Third", @workflow, @user)
    assert_equal 2, comment3.events.first.notifications.count
    # @user wrote a comment, so their notification about comment1 is done now
    assert comment1.events.first.notifications.find_by(user: @user).done?
    # @active_user received a notification about comment3, but their notification about comment 2 is not archived
    refute comment2.events.first.notifications.find_by(user: @active_user).done?

    comment4 = Interactors::CommentInteractor.create("Fourth", @workflow, @active_user)
    assert_equal 2, comment4.events.first.notifications.count
    # @active_user received a notification about comment3, but their notification about comment 2 is not archived
    assert comment2.events.first.notifications.find_by(user: @active_user).done?
    assert comment3.events.first.notifications.find_by(user: @active_user).done?
    refute comment3.events.first.notifications.find_by(user: @informed_user).done?
    refute comment4.events.first.notifications.find_by(user: @informed_user).done?
    refute comment4.events.first.notifications.find_by(user: @user).done?
  end

  test "writing a comment doesn't archive bookmarked notifications" do
    comment1 = Interactors::CommentInteractor.create("First", @workflow, @active_user)
    notification = comment1.events.first.notifications.find_by(user: @informed_user)
    notification.update(bookmarked_at: Time.now)
    Interactors::CommentInteractor.create("Second", @workflow, @informed_user)
    refute comment1.events.first.notifications.find_by(user: @informed_user).done?
  end
end
