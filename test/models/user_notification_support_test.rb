require "test_helper"

class UserNotificationSupport < ActiveSupport::TestCase
  test "scheduling next delivery with predefined values" do
    any_user = User.first
    assert_equal 1, any_user.noti_interval # the default interval

    test_time = Time.new(2021)
    Timecop.freeze(test_time) do
      # never
      any_user.schedule_next_delivery(0)
      assert_nil any_user.noti_next_send_at, "never send recent notifications mail"

      # immediately
      any_user.schedule_next_delivery(1)
      assert_equal Time.parse("2021-01-01T00:01:00"), any_user.noti_next_send_at, "send in the next minute"

      # 1 hour
      any_user.schedule_next_delivery(3600)
      assert_equal Time.parse("2021-01-01T01:00:00"), any_user.noti_next_send_at, "send at the full hour"

      # 1 day -> next day at 6:00
      any_user.schedule_next_delivery(86400)
      assert_equal Time.parse("2021-01-02T06:00:00"), any_user.noti_next_send_at, "send next day at 6:00"

      # 1 week -> next monday at 6:00
      any_user.schedule_next_delivery(604800)
      assert_equal Time.parse("2021-01-04T06:00:00"), any_user.noti_next_send_at, "send next monday at 6:00"
    end
  end

  test "scheduling next delivery with custom values" do
    any_user = User.first
    test_time = Time.new(2021)
    Timecop.freeze(test_time) do
      # custom -> seconds, but rounded down to the minute
      any_user.noti_next_send_at = nil # reset
      any_user.schedule_next_delivery(1234)
      assert_equal Time.parse("2021-01-01T00:20:00"), any_user.noti_next_send_at

      any_user.noti_next_send_at = nil # reset
      any_user.schedule_next_delivery(12345)
      assert_equal Time.parse("2021-01-01T03:25:00"), any_user.noti_next_send_at

      any_user.noti_next_send_at = nil # reset
      any_user.schedule_next_delivery(123456)
      assert_equal Time.parse("2021-01-02T10:17:00"), any_user.noti_next_send_at

      any_user.noti_next_send_at = nil # reset
      any_user.schedule_next_delivery(1234567)
      assert_equal Time.parse("2021-01-15T06:56:00"), any_user.noti_next_send_at
    end
  end
end
