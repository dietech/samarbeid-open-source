require "test_helper"

class RunReindexForWorkflowsJobTest < ActiveJob::TestCase
  test "running job should work" do
    workflow_definition_with_workflows = WorkflowDefinition.all.find { |wd| wd.workflows.count > 0 }
    assert_nothing_raised { RunReindexForWorkflowsJob.perform_now(workflow_definition_with_workflows) }
  end
end
