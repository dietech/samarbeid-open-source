require "test_helper"

class CleanupInvalidDataJobTest < ActiveJob::TestCase
  test "running job should work" do
    assert_nothing_raised { CleanupInvalidDataJob.perform_now }
  end
end
