require "test_helper"

class GoodJobSettingsTest < ActiveJob::TestCase
  test "running failing job should not lead to infinite loop" do
    assert_nothing_raised do
      perform_enqueued_jobs do
        JobWhichAlwaysFailsJob.perform_later
      end
    end
  end

  test "running job not inheriting from ApplicationJob should work" do
    assert_nothing_raised do
      perform_enqueued_jobs do
        MailerAlwaysFailing.perform_later
      end
    end
  end
end

class MailerAlwaysFailing < ActionMailer::MailDeliveryJob
  def perform
    raise "BOOM"
  end
end

class JobWhichAlwaysFailsJob < ApplicationJob
  def perform
    raise "BOOM"
  end
end
