require "application_system_test_case"
require "system_test_helper"

class DsbMeldungSystemTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper

  setup do
    # Run all jobs during this Smoke Test to update visibilies etc.
    ActiveJob::Base.queue_adapter.perform_enqueued_jobs = true
    ActiveJob::Base.queue_adapter.perform_enqueued_at_jobs = true
  end

  teardown do
    ActiveJob::Base.queue_adapter.perform_enqueued_jobs = false
    ActiveJob::Base.queue_adapter.perform_enqueued_at_jobs = false
  end

  test "DSB-Missbrauchsmeldung Smoke-Test" do
    # Schritt 1 (fehlerhafte Anmeldung)
    visit root_url
    login_using_form_with("dsb@example.org", "das falsche passwort")
    assert_text "E-Mail oder Passwort ungültig."

    # Schritt 2 (erfolgreiche Anmeldung)
    login_using_form_with("dsb@example.org", "password")
    assert_button("user-menu") # We are logged in

    # Schritt 3 (Starten einer Maßnahme zur Verarbeitung der Missbrauchsmeldung)
    within(header_area) { click_on "Maßnahmen" }
    click_on "Maßnahme starten"
    create_workflow_dialog = page.find(".create-workflow-dialog", match: :first)
    within(create_workflow_dialog) do
      click_list_item_link("NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
      fill_in("Titel der Maßnahme", with: "Test-Maßnahme")
      click_card_action_button("MASSNAHME STARTEN")
    end
    assert_current_path(/tasks\/\d+/)

    within left_sidebar do
      click_on "Test-Maßnahme"
    end

    assert within(right_sidebar) { control_for("Verantwortlicher").has_text?("dsb@example.org") }

    assert_browser_logs_empty

    # Schritt 5 (Ziel zuweisen)
    within right_sidebar do
      within control_for("Ziele") do
        click_on "Ändern"
        fill_in "Suche", with: "Missbrauchsmeldungen"
        find(".v-list-item", text: "Keine offenen Missbrauchsmeldungen").click
      end
      assert control_for("Ziele").has_text?("Keine offenen Missbrauchsmeldungen")
    end

    assert_browser_logs_empty

    # Schritt 6 (Titel und Beschreibung)
    open_title_and_description_card
    new_title = "Oper Leipzig"
    new_description = "Verkauf von Ehrenamtskarten der Oper Leipzig."
    fill_in_title_and_description new_title, new_description
    click_title_and_description_edit_button "SPEICHERN"

    assert_text new_title
    assert_text new_description

    assert_browser_logs_empty

    # Schritt 7 (Beginn der Eingabe der Daten für die erste Aufgabefehlerhafte Mailadresse eingeben)
    within(left_sidebar) { click_on("Missbrauchshinweis aufnehmen") }
    assert middle_area.has_text?("Missbrauchshinweis aufnehmen")
    retry_flaky_actions {
      click_on "Aufgabe starten"
      assert_css ".page-detail-header .v-chip__content", text: "Aktiv"
    }
    within right_sidebar do
      assert control_for("Verantwortlicher").has_text?("dsb@example.org")
    end

    within(middle_area) do
      # Controls von aktiven Aufgaben sollen nicht readonly sein
      assert has_control?(with_label: "E-Mail-Adresse Absender", is_readonly: false)

      within(find(".v-input", text: "E-Mail-Adresse Absender")) do
        assert_browser_logs_empty
        fill_in "E-Mail-Adresse Absender", with: "somebody@emailprovider\t"
        assert has_text? "muss eine E-Mail-Adresse sein"
        expect_console_log_error(message: "/data - Failed to load resource: the server responded with a status of 422 (Unprocessable Entity)")
      end
    end

    assert_browser_logs_empty

    # Schritt 8 (Vollständige Eingabe der Daten für die Aufgabe Missbrauchshinweis aufnehmen)
    within(middle_area) do
      within(find(".v-input", text: "E-Mail-Adresse Absender")) do
        fill_in "E-Mail-Adresse Absender", with: "somebody@emailprovider.co.uk"
      end
      within(find(".v-input", text: "Text der E-Mail")) do
        fill_in "Text der E-Mail", with: <<~EOF
          Es handelt sich hierbei um eine Veranstaltung bei der ehrenamtlich Tätige eingeladen werden, um deren Einsatz und Engagement zu würdigen. 
          Die Einladungen sind personengebunden. Eine Weitergabe der Einladung und Eintrittskarten an Dritte ist nicht vorgesehen.
          Ich appelliere daher an Sie, das o.g. Inserat zu löschen.
          
          Mit freundlichen Grüßen
          Im Auftrag
          Dr. Prof. Sombody
        EOF
      end
      within(find(".v-input", text: "Link zur betroffenen Anzeige")) do
        fill_in "Link zur betroffenen Anzeige", with: "https://www.example.org/784406"
      end
      within(find(".v-input", text: "Text der betroffenen Anzeige")) do
        fill_in "Text der betroffenen Anzeige", with: "Hallo\nich biete 2 Eintrittskarten für die das Event am 04.12.2019 um 18.30 Uhr an."
      end
    end

    assert_browser_logs_empty

    #     Schritt 9 (Aufgabe abschließen und Weiterleiten an Nutzer1)
    within middle_area do
      assert has_text?("Missbrauchshinweis aufnehmen")
      click_on "Aufgabe abschließen"
    end

    within left_sidebar do
      next_task_in_sidebar = find(".v-list-item", text: "Entscheidung Löschung")
      assert next_task_in_sidebar.has_text?("Wartet")
    end
    assert middle_area.has_text?("Entscheidung Löschung") # Automatisch zur nächsten Aufgabe weitergeleitet

    within right_sidebar do
      within(control_for("Verantwortlicher")) do
        click_on "Ändern"
        fill_in "Suche", with: "Boss"
        find(".v-list-item", text: "dsb-boss@example.org").click
      end
      assert control_for("Verantwortlicher").has_text?("dsb-boss@example.org")
    end

    within(middle_area) do
      retry_flaky_actions {
        click_on "Aufgabe starten"
        assert_css ".page-detail-header .v-chip__content", text: "Aktiv"
      }
    end

    comment_area = find_prosemirror_editor(page.find(".activity-hub"))
    prosemirror_fill_in(comment_area, "@dsb-bo")
    page.find(".mention-suggestions-menu .v-list-item", text: "dsb-boss@example.org").click
    prosemirror_fill_in(comment_area, "kannst Du bitte noch heute diese Entscheidung vornehmen. Danke.")
    click_button("Kommentieren")

    assert_browser_logs_empty
    logout_using_menu

    # Schritt 10 (Nutzer1 trifft die Entscheidung, dass diese Anzeige gelöscht werden soll)
    login_using_form_with("dsb-boss@example.org", "password")
    click_on("user-tasks-button")
    click_on("Entscheidung Löschung")

    tasks_and_workflow_visible = left_sidebar.all("a.v-list-item")
    assert_equal 3, tasks_and_workflow_visible.count # We see all task as we are part of the group (we used to allow users to only see single tasks)

    within(middle_area) do
      assert_no_button "Aufgabe abschließen"
      within(find(".v-input", text: "Deine kurze Begründung für die Entscheidung")) do
        fill_in "Deine kurze Begründung für die Entscheidung", with: "Offensichtlich ist es nicht gewünscht, dass diese Karten verkauft werden. Daher löschen wir die Anzeige."
      end
      within(find(".v-input", text: "Soll die Anzeige gelöscht werden?")) do
        click_on "Ja"
        has_text? "Ja" # wait until spinner disappears
      end
      click_on "Aufgabe abschließen"
    end
    # we are redirected to next task which is now ready
    assert middle_area.has_text?("Anzeige löschen")
    assert middle_area.has_text?("Wartet")

    assert_browser_logs_empty

    logout_using_menu

    # Schritt 15 (Benachrichtigung sehen und zu nächster Aufgabe gehen)
    login_using_form_with("dsb@example.org", "password")
    click_on("user-notifications-button")
    notification_on_finished_task = find(".v-main .v-timeline-item", text: /DSB Boss hat Aufgabe #\d+ • Entscheidung Löschung in Maßnahme %\d+ • Oper Leipzig abgeschlossen/)
    within(notification_on_finished_task) { click_on "Oper Leipzig" }

    within(left_sidebar) { click_on("Anzeige löschen") }
    assert middle_area.has_text?("Anzeige löschen")

    assert_browser_logs_empty

    # Schritt 16 (Anzeige Löschen und Dankes-Email-Schreiben)
    within right_sidebar do
      within(control_for("Verantwortlicher")) do
        click_on "Ändern"
        find(".v-list-item", text: "dsb@example.org").click
      end
      assert control_for("Verantwortlicher").has_text?("dsb@example.org")
    end
    within middle_area do
      retry_flaky_actions {
        click_on "Aufgabe starten"
        assert_css ".page-detail-header .v-chip__content", text: "Aktiv"
      }

      assert has_text?("Anzeige löschen")
      assert has_infobox?(with_label: "Link zur betroffenen Anzeige", with_value: "https://www.example.org/784406")
      click_on "Aufgabe abschließen"
    end

    assert middle_area.has_text?("Versendung Standard-Dankes-E-Mail")
    within right_sidebar do
      within(control_for("Verantwortlicher")) do
        click_on "Ändern"
        find(".v-list-item", text: "dsb@example.org").click
      end
      assert control_for("Verantwortlicher").has_text?("dsb@example.org")
    end

    within middle_area do
      retry_flaky_actions {
        click_on "Aufgabe starten"
        assert_css ".page-detail-header .v-chip__content", text: "Aktiv"
      }

      assert has_text?("Versendung Standard-Dankes-E-Mail")
      assert has_infobox?(with_label: "E-Mail-Adresse Absender", with_value: "somebody@emailprovider.co.uk")
      assert has_infobox?(with_label: "Versendung Standard-Dankes-E-Mail", with_value: "Lieber Nutzer, danke für den Hinweis. Wir haben die Anzeige gelöscht. Viele Grüße")
      click_on "Aufgabe abschließen"
    end

    assert middle_area.has_text?("NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")

    middle_area.click_on "Maßnahme abschließen"
    assert middle_area.has_text?("Abgeschlossen")
  end
end
