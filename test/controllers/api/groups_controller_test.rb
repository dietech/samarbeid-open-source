require "test_helper"

class Api::GroupsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    @admin = User.find_by!(email: "team-admin@example.org")
  end

  test "admin should get index of groups" do
    CustomElasticSearchConfig.reindex_and_refresh(Group)
    login_as @admin
    get api_groups_path, as: :json
    assert_response :success
  end

  test "non admin shouldn't get index of groups" do
    CustomElasticSearchConfig.reindex_and_refresh(Group)
    login_as @user
    get api_groups_path, as: :json
    assert_response :forbidden
  end

  test "non admin shouldn't get a group which he doesn't belong to" do
    login_as @user
    get api_group_path(Group.find_by!(name: "tomsy")), as: :json
    assert_response :forbidden
  end

  test "non admin should get a group which he does belong to" do
    login_as @user
    get api_group_path(@user.groups.first), as: :json
    assert_response :success
  end

  test "non admin shouldn't create a group" do
    login_as @user
    post api_groups_path, as: :json, params: {
      name: "Test group"
    }
    assert_response :forbidden
  end

  test "admin should create a group" do
    login_as @admin
    post api_groups_path, as: :json, params: {
      name: "Test group",
      description: "Test Description",
      user_ids: User.all.limit(5).pluck(:id),
      workflow_definition_ids: WorkflowDefinition.all.limit(5).pluck(:id)
    }
    assert_response :success
  end

  test "should send an error while creating a group with non valid attributes" do
    login_as @admin
    post api_groups_path, as: :json, params: {
      name: ""
    }
    assert_response :unprocessable_entity
  end

  test "non admin shouldn't update a group" do
    login_as @user
    patch api_group_path(Group.find_by!(name: "tomsy")), as: :json, params: {
      name: "Test group"
    }
    assert_response :forbidden
  end

  test "admin should update a group" do
    login_as @admin
    patch api_group_path(Group.find_by!(name: "tomsy")), as: :json, params: {
      name: "Test group",
      description: "Test Description",
      user_ids: User.all.limit(5).pluck(:id),
      workflow_definition_ids: WorkflowDefinition.all.limit(5).pluck(:id)
    }
    assert_response :success
  end

  test "should send an error while updating a group with non valid attributes" do
    login_as @admin
    patch api_group_path(Group.find_by!(name: "tomsy")), as: :json, params: {
      name: ""
    }
    assert_response :unprocessable_entity
  end

  test "non admin shouldn't destroy a group" do
    login_as @user
    delete api_group_path(Group.find_by!(name: "tomsy")), as: :json
    assert_response :forbidden
  end

  test "admin should destroy a group" do
    login_as @admin
    delete api_group_path(Group.find_by!(name: "tomsy")), as: :json
    assert_response :success
  end

  test "should send an error while deleting a not existing group" do
    login_as @admin
    delete api_group_path(id: Group.maximum(:id).next), as: :json
    assert_response :not_found
  end
end
