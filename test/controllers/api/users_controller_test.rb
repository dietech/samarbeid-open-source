require "test_helper"

class Api::UsersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    @admin = User.find_by!(email: "team-admin@example.org")
  end

  test "non admin shouldn't get index of users" do
    CustomElasticSearchConfig.reindex_and_refresh(User)
    login_as @user
    get api_users_path(format: :json)
    assert_response :forbidden
  end

  test "admin should get index of users" do
    CustomElasticSearchConfig.reindex_and_refresh(User)
    login_as @admin
    get api_users_path(format: :json)
    assert_response :success
  end

  test "should get filtered list of users" do
    login_as @user
    get list_api_users_path(except: [@user.id], group_ids: [Group.first], query: "admin", format: :json)
    assert_response :success
  end

  test "list of users shouldn't include deactivated users" do
    @user.deactivate
    login_as @admin
    get list_api_users_path(format: :json)
    assert_response :success

    response_array = JSON.parse(response.body)
    assert_equal User.active.count, response_array.length
    refute response_array.any? { |u| u[:id] == @user.id }
  end

  test "should get other user" do
    login_as @user
    get api_user_path(@admin, format: :json)
    assert_response :success
  end

  test "unauthorized user shouldn't get current user info" do
    get info_api_users_path(format: :json)
    assert_response :unauthorized
  end

  test "should get current user info" do
    login_as @user
    get info_api_users_path(format: :json)
    assert_response :success
  end

  test "admin should create an user" do
    login_as @admin
    post api_users_path, as: :json, params: {
      email: "test@test.org",
      firstname: "Test",
      lastname: "User",
      password: "password",
      password_confirmation: "password"
    }
    assert_response :success
  end

  test "non admin shouldn't create an user" do
    login_as @user
    post api_users_path, as: :json, params: {
      email: "test@test.org",
      firstname: "Test",
      lastname: "User",
      password: "password",
      password_confirmation: "password"
    }
    assert_response :forbidden
  end

  test "admin may not make himself non-admin" do
    login_as @admin
    patch update_admin_status_api_user_setting_path(@admin), as: :json, params: {admin: false}
    assert_response :forbidden
  end

  test "admin may make other user admin" do
    login_as @admin
    patch update_admin_status_api_user_setting_path(@user), as: :json, params: {admin: true}
    assert_response :success
  end

  test "non admin may not make other user admin" do
    login_as @user
    patch update_admin_status_api_user_setting_path(@user), as: :json, params: {admin: false}
    assert_response :forbidden
  end

  test "deactivated user shouldn't be able to login" do
    @user.deactivate

    post user_session_path, params: {
      user: {
        email: @user.email,
        password: "password"
      }
    }
    assert_redirected_to new_user_session_path

    refute @request.env["warden"].authenticated?
  end
end
