def login_using_form_with(email, password = "password")
  assert has_text?("Anmelden")

  within("form#sign-in") do
    fill_in("user[email]", with: email)
    fill_in("user[password]", with: password)
    click_button("Anmelden")
  end
end

def logout_using_menu
  find_button("user-menu").click
  assert has_text?("Abmelden")

  list_of_user_menu_items = find("#user-menu-list")
  logout_link = list_of_user_menu_items.find("a", text: "Abmelden")
  logout_link.click

  assert_current_path(new_user_session_path)
  assert has_text?("Anmelden")
end

def set_assignee(user)
  within(control_for("Verantwortlicher")) { click_button("Ändern") }
  page.find(".v-menu__content .v-list .v-list-item--link", text: user.name).click
end

def control_for(text)
  page.find(".v-input.custom-object-control", text: text)
end

def header_area
  page.find("header.v-toolbar")
end

def right_sidebar
  page.find(".v-navigation-drawer--right")
end

def left_sidebar
  page.find(".v-navigation-drawer", match: :first)
end

def middle_area
  page.find(".container", match: :first)
end

def find_text(text)
  find("*", text: text)
end

# Probably supports only controls with a form field (input, textarea, ...) for the value
def has_infobox?(with_label:, with_value: nil)
  has_field_options = {
    disabled: true
  }
  has_field_options[:with] = with_value unless with_value.nil?

  find(".support-infobox .v-input", text: with_label).has_field?(**has_field_options)
end

def has_control?(with_label: nil, with_value: nil, is_readonly: false)
  selector = ".v-input" + (is_readonly ? ".v-input--is-disabled.control--is-readonly" : "")

  find_options = {}
  find_options[:text] = with_label unless with_label.nil?

  has_field_options = {
    disabled: is_readonly
  }
  has_field_options[:with] = with_value unless with_value.nil?

  find(selector, **find_options).has_field?(**has_field_options)
end

def open_vertical_dots_context_menu
  page.find("button .v-icon.mdi-dots-vertical").click
end

def open_title_and_description_card
  page.find("button .v-icon.mdi-square-edit-outline").click
end

def find_prosemirror_editor(parent)
  parent.find(".tiptap-vuetify-editor .ProseMirror")
end

def prosemirror_fill_in(element, text)
  element.base.send_keys(text)
end

def fill_in_title_and_description(title, description)
  unless title.nil?
    page.find(".v-card .v-input .v-text-field__slot .v-label", text: "Titel")
      .find(:xpath, "..")
      .find("input[type=text]")
      .fill_in with: title
  end

  unless description.nil?
    editor_parent = page.find(".v-card .richtextarea .v-text-field__slot .v-label", text: "Beschreibung")
      .find(:xpath, "..")

    prosemirror_fill_in(find_prosemirror_editor(editor_parent), description)
  end
end

def click_title_and_description_edit_button(text)
  page.find(".v-card .v-card__actions button", text: text).click
end

def has_menu_list_item(text, disabled = false)
  page.has_css?(".v-menu__content .v-list #{disabled ? ".v-list-item--disabled" : ".v-list-item--link"}", text: text)
end

def click_menu_list_item(text)
  page.find(".v-menu__content .v-list .v-list-item--link", text: text).click
end

def click_dialog_button(text)
  page.find(".v-dialog button", text: text).click
end

def click_list_item_link(text)
  page.find(".v-list .v-list-item--link", text: text).click
end

def click_card_action_button(text)
  find(".v-card .v-card__actions button", text: text).click
end

def click_tab(text)
  page.find(".v-tab", text: text).click
end

def click_ambition_workflow_select_button
  page.find(".custom-object-control .v-card .v-card__title div", text: "Zu erledigende Maßnahmen")
    .find(:xpath, "..")
    .find("button", text: "ÄNDERN")
    .click
end
